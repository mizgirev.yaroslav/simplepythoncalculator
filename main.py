from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5 import QtGui

class myCalculator(QtWidgets.QWidget):
    
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
#        переменная для отображения действий пользователя
        self.label = QtWidgets.QLabel("")
        self.label.setAlignment(QtCore.Qt.AlignRight)
        self.label.setFont(QtGui.QFont('Times', 10))
#        переменная для ввода данных от пользователя
        self.input_field = QtWidgets.QLineEdit()
#        переменная - сетка
        self.grid = None
#        привязка событий к виджетам
        self.__mapper = QtCore.QSignalMapper(self)
#        слои
        self.main_layer = QtWidgets.QVBoxLayout()
        self.result_layer = QtWidgets.QHBoxLayout()
        self.data_layer = QtWidgets.QHBoxLayout()
        self.buttons_layer = QtWidgets.QHBoxLayout()
        self.num_buttons_layer = QtWidgets.QVBoxLayout()
        self.math_buttons_layer = QtWidgets.QVBoxLayout()
        self.main_layer.addLayout(self.result_layer)
        self.main_layer.addLayout(self.data_layer)
        self.main_layer.addLayout(self.buttons_layer)
        self.buttons_layer.addLayout(self.num_buttons_layer)
        self.buttons_layer.addLayout(self.math_buttons_layer)
#        добавляем виджеты
        self.result_layer.addWidget(self.label)
        self.data_layer.addWidget(self.input_field)
#        установка выравнивания в поле - ввода данных
        self.input_field.setAlignment(QtCore.Qt.AlignRight)
#        добавляем кнопки на buttons_layer
        self.calc_numbers()
#        установка основного слоя окна
        self.setLayout(self.main_layer)

    def calc_numbers(self):
        num_grid = QtWidgets.QGridLayout()
        self.num_buttons_layer.addLayout(num_grid)
#        установка кнопок с числами
        for i in range(0, 10):
            btn = QtWidgets.QPushButton(str(i))
            num_grid.addWidget(btn, ((i+1)-1)//3, ((i+1)-1)%3)
            btn.clicked.connect(self.__mapper.map)
            self.__mapper.setMapping(btn, i)
#        привязка событий к кнопкам
        self.__mapper.mapped['int'].connect(self.slot_on_num_button_clicked)
#        слой для операторов
        math_grid = QtWidgets.QGridLayout()
        self.math_buttons_layer.addLayout(math_grid)
#        установка операторов
        names = ['+', '-', '*', '/', 'C', "Del", '=']
        for i, name in enumerate(names, 1):
            btn = QtWidgets.QPushButton(name)
            btn.clicked.connect(self.operation_clicked)
            math_grid.addWidget(btn, (i - 1) // 2, (i - 1) % 2)

    def operation_clicked(self):
        button_text = self.sender().text()
#        при нажатии Del
        if button_text == "Del":
            self.input_field.setText(self.input_field.text()[:-1])
#        при нажатии С
        elif button_text == "C":
            self.input_field.setText("")
#        при нажатии =
        elif button_text == "=":
            if self.isCorrect(self.input_field.text()):
                self.label.setText(str(eval(self.input_field.text())))
                self.input_field.setText("")
            else:
                self.input_field.setText("")
#        при нажатии прочих кнопок с операторами
        else:
            self.input_field.setText(self.input_field.text() + self.sender().text())

    def slot_on_num_button_clicked(self, num: int):
        self.input_field.setText(self.input_field.text() + str(num))

    def isCorrect(self, string: str):
        """функция возвращает False в случае ошибки и выводит описание самой ошибки"""
        flag = 0
        division = 0
#        проверка первого символа в выражении
        if string[0] in {'+', '-', '*', '/'}:
            msg = QtWidgets.QMessageBox()
            msg.setText("Error:")
            msg.setInformativeText("    The first symbol is operator: " + string[0])
            msg.setWindowTitle("Error")
            msg.exec_()
            return False
#        разбор выражения посимвольно
        for symbol in string:
#            если символ удовлетворяет условиям задачи
            if symbol.isdigit() or symbol.isspace() or symbol in {'+', '-', '*', '/'}:
#                определение арифметического действия
                if symbol in {'+', '-', '*', '/'} and flag == 0:
                    if symbol == '/':
                        division = 1
                    flag = 1
#                проверка на последовательность арифметических операторов
                elif symbol in {'+', '-', '*', '/'} and flag == 1:
                    msg = QtWidgets.QMessageBox()
                    msg.setText("Error:")
                    msg.setInformativeText("    Two consecutive statements")
                    msg.setWindowTitle("Error")
                    msg.exec_()
                    return False
                else:
#                    проверка деления на ноль
                    if division == 1:
                        if int(symbol) == 0:
                            msg = QtWidgets.QMessageBox()
                            msg.setText("Error:")
                            msg.setInformativeText('    Division by zero')
                            msg.setWindowTitle("Error")
                            msg.exec_()
                            return False
                        else:
                            division = 0
                    flag = 0
#            если символ не удовлетворяет условиям задачи
            else:
                msg = QtWidgets.QMessageBox()
                msg.setText("Error:")
                msg.setInformativeText("    Wrong symbol: " + symbol)
                msg.setWindowTitle("Error")
                msg.exec_()
                return False
#        проверка последнего символа
        if symbol in {'+', '-', '*', '/'}:
            msg = QtWidgets.QMessageBox()
            msg.setText("Error:")
            msg.setInformativeText("    Last symbol is operator without number")
            msg.setWindowTitle("Error")
            msg.exec_()
            return False
#        выражение без ошибок
        return True

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = myCalculator()
    window.setWindowTitle("MyCalc")
    window.resize(300, 70)
    window.show()
    sys.exit(app.exec_())
